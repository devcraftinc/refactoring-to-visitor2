﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
namespace Production
{
    public class MostImportantTargetSelector
    {
        private MostImportantTargetSelector() { }

        public static MostImportantTargetSelector GetInstance()
        {
            return new MostImportantTargetSelector();
        }

        public Hitable SelectTarget(GameObject gameObject)
        {
            if (gameObject is Boat b)
            {
                Hitable weakest = b.BowArmor;
                weakest = ChooseWeaker(weakest, b.SternArmor);
                weakest = ChooseWeaker(weakest, b.PortArmor);
                weakest = ChooseWeaker(weakest, b.StarboardArmor);

                return weakest;
            }

            if (gameObject is Fortress f)
            {
                Hitable weakest = null;
                weakest = ChooseWeakerButStillActive(weakest, f.Turret1);
                weakest = ChooseWeakerButStillActive(weakest, f.Turret2);

                if (weakest == null)
                    weakest = f.Walls;

                return weakest;
            }

            if (gameObject is Army a)
                return a;

            return null;
        }

        private Hitable ChooseWeakerButStillActive(Hitable left, Hitable right)
        {
            if (right.HitPoints > 0)
                return ChooseWeaker(left, right);

            return left;
        }

        private Hitable ChooseWeaker(Hitable left, Hitable right)
        {
            if (left == null)
                return right;

            if (right.HitPoints < left.HitPoints)
                return right;

            return left;
        }
    }
}
