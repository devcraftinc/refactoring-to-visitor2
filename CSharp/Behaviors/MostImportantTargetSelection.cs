﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Production;

namespace Behaviors
{
    [TestClass]
    public class MostImportantTargetSelection
    {
        private MostImportantTargetSelector selector;

        [TestInitialize]
        public void Setup()
        {
            selector = MostImportantTargetSelector.GetInstance();
        }

        [TestMethod]
        public void ArmyIsAlwaysOnlyTarget()
        {
            var army = Army.GetInstance();

            var selected = selector.SelectTarget(army);

            Assert.AreSame(army, selected);
        }

        [TestMethod]
        public void WeakestSideIsMostImportantPartOfBoat()
        {
            var normalArmor = 100;
            var boat = Boat.GetInstance();
            ChoosePartOfBoatSpecification(boat, normalArmor - 1, normalArmor, normalArmor, normalArmor, boat.BowArmor);
            ChoosePartOfBoatSpecification(boat, normalArmor, normalArmor - 1, normalArmor, normalArmor, boat.SternArmor);
            ChoosePartOfBoatSpecification(boat, normalArmor, normalArmor, normalArmor - 1, normalArmor, boat.PortArmor);
            ChoosePartOfBoatSpecification(boat, normalArmor, normalArmor, normalArmor, normalArmor - 1, boat.StarboardArmor);
        }

        [TestMethod]
        public void PrioritizesWeakestCanonOverArmorForFortress()
        {
            var normalArmor = 100;
            var fortress = Fortress.GetInstance();
            ChoosePartOfFortressSpecification(fortress, Any.Integer(), normalArmor - 1, normalArmor, fortress.Turret1);
            ChoosePartOfFortressSpecification(fortress, Any.Integer(), normalArmor, normalArmor - 1, fortress.Turret2);
        }

        [TestMethod]
        public void IgnoresDestroyedTurrestForFortress()
        {
            var destroyedArmor = 0;
            var fortress = Fortress.GetInstance();
            ChoosePartOfFortressSpecification(fortress, Any.Integer(), destroyedArmor + 1, destroyedArmor, fortress.Turret1);
            ChoosePartOfFortressSpecification(fortress, Any.Integer(), destroyedArmor, destroyedArmor + 1, fortress.Turret2);
        }

        [TestMethod]
        public void SelectsWallsIfBothTurrestDestroyedForFortress()
        {
            var destroyedArmor = 0;
            var fortress = Fortress.GetInstance();
            ChoosePartOfFortressSpecification(fortress, destroyedArmor + 1, destroyedArmor, destroyedArmor, fortress.Walls);
        }

        [TestMethod]
        public void UnknownUnitTypeYieldsNoTargets()
        {
            Assert.IsNull(selector.SelectTarget(new UnknownGameObject()));
        }

        private void ChoosePartOfBoatSpecification(Boat boat, int bow, int stern, int port, int starboard, Armor expectedTarget)
        {
            boat.BowArmor.HitPoints = bow;
            boat.SternArmor.HitPoints = stern;
            boat.PortArmor.HitPoints = port;
            boat.StarboardArmor.HitPoints = starboard;

            var actual = selector.SelectTarget(boat);

            Assert.AreSame(expectedTarget, actual);
        }

        private void ChoosePartOfFortressSpecification(Fortress fortress, int walls, int turret1, int turret2, Armor expected)
        {
            fortress.Walls.HitPoints = walls;
            fortress.Turret1.HitPoints = turret1;
            fortress.Turret2.HitPoints = turret2;

            var actual = selector.SelectTarget(fortress);

            Assert.AreSame(expected, actual);
        }
    }
}