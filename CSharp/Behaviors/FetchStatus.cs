﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Production;

namespace Behaviors
{
    [TestClass]
    public class FetchStatus
    {
        private StatusEngine statusEngine;

        [TestInitialize]
        public void Setup()
        {
            statusEngine = StatusEngine.GetInstance();
        }

        [TestMethod]
        public void BoatStatus()
        {
            var boat = Boat.GetInstance();
            boat.Lat = Any.Double();
            boat.Lon = Any.Double();
            boat.Heading = Any.Double();
            boat.SailsUp = Any.YesNo();
            boat.CannonBalls = Any.Integer();
            boat.BowArmor.HitPoints = Any.Integer();
            boat.SternArmor.HitPoints = Any.Integer();
            boat.PortArmor.HitPoints = Any.Integer();
            boat.StarboardArmor.HitPoints = Any.Integer();

            var status = statusEngine.GetStatus(boat);

            Assert.AreEqual(
$@"Position:     {boat.Lat}, {boat.Lon}
Heading:       {boat.Heading}
Sails Up:      {boat.SailsUp}
Ammo:          {boat.CannonBalls}
Armor:
  - Bow:       {boat.BowArmor.HitPoints}
  - Stern:     {boat.SternArmor.HitPoints}
  - Port:      {boat.PortArmor.HitPoints}
  - Starboard: {boat.StarboardArmor.HitPoints}",
                status);
        }

        [TestMethod]
        public void FortressStatus()
        {
            var fortress = Fortress.GetInstance();
            fortress.Lat = Any.Double();
            fortress.Lon = Any.Double();
            fortress.CannonBalls = Any.Integer();
            fortress.Walls.HitPoints = Any.Integer();
            fortress.Turret1.HitPoints = Any.Integer();
            fortress.Turret2.HitPoints = Any.Integer();

            var status = statusEngine.GetStatus(fortress);

            Assert.AreEqual(
                $@"Position:     {fortress.Lat}, {fortress.Lon}
Ammo:          {fortress.CannonBalls}
Armor:         {fortress.Walls.HitPoints}
Turret 1:      {fortress.Turret1.HitPoints}
Turret 2:      {fortress.Turret2.HitPoints}",
                status);
        }

        [TestMethod]
        public void ArmyStatus()
        {
            var army = Army.GetInstance();
            army.Lat = Any.Double();
            army.Lon = Any.Double();
            army.Supplies = Any.Integer();
            army.Soldiers = Any.Integer();

            var status = statusEngine.GetStatus(army);

            Assert.AreEqual(
                $@"Position:     {army.Lat}, {army.Lon}
Ammo:          {army.Supplies}
Soldiers:      {army.Soldiers}",
                status);
        }

        [TestMethod]
        public void UnknownType()
        {
            var other = new UnknownGameObject();

            var status = statusEngine.GetStatus(other);

            Assert.AreEqual("(unknown)", status);
        }
    }
}
