// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

public class MostImportantTargetSelection {
    private MostImportantTargetSelector selector;

    @Before
    public void setup() {
        selector = MostImportantTargetSelector.getInstance();
    }

    @Test
    public void armyIsAlwaysOnlyTarget() {
        Army army = Army.getInstance();

        Hitable selected = selector.selectTarget(army);

        assertSame(army, selected);
    }

    @Test
    public void weakestSideIsMostImportantPartOfBoat() {
        int normalArmor = 100;
        Boat boat = Boat.getInstance();
        choosePartOfBoatSpecification(boat, normalArmor - 1, normalArmor, normalArmor, normalArmor, boat.getBowArmor());
        choosePartOfBoatSpecification(boat, normalArmor, normalArmor - 1, normalArmor, normalArmor, boat.getSternArmor());
        choosePartOfBoatSpecification(boat, normalArmor, normalArmor, normalArmor - 1, normalArmor, boat.getPortArmor());
        choosePartOfBoatSpecification(boat, normalArmor, normalArmor, normalArmor, normalArmor - 1, boat.getStarboardArmor());
    }

    @Test
    public void prioritizesWeakestCanonOverArmorForFortress() {
        int normalArmor = 100;
        Fortress fortress = Fortress.GetInstance();
        choosePartOfFortressSpecification(fortress, Any.integerValue(), normalArmor - 1, normalArmor, fortress.getTurret1());
        choosePartOfFortressSpecification(fortress, Any.integerValue(), normalArmor, normalArmor - 1, fortress.getTurret2());
    }

    @Test
    public void ignoresDestroyedTurrestForFortress() {
        int destroyedArmor = 0;
        Fortress fortress = Fortress.GetInstance();
        choosePartOfFortressSpecification(fortress, Any.integerValue(), destroyedArmor + 1, destroyedArmor, fortress.getTurret1());
        choosePartOfFortressSpecification(fortress, Any.integerValue(), destroyedArmor, destroyedArmor + 1, fortress.getTurret2());
    }

    @Test
    public void selectsWallsIfBothTurrestDestroyedForFortress() {
        int destroyedArmor = 0;
        Fortress fortress = Fortress.GetInstance();
        choosePartOfFortressSpecification(fortress, destroyedArmor + 1, destroyedArmor, destroyedArmor, fortress.getWalls());
    }

    @Test
    public void unknownUnitTypeYieldsNoTargets() {
        assertNull(selector.selectTarget(new UnknownGameObject()));
    }

    private void choosePartOfBoatSpecification(Boat boat, int bow, int stern, int port, int starboard, Armor expectedTarget) {
        boat.getBowArmor().setHitPoints(bow);
        boat.getSternArmor().setHitPoints(stern);
        boat.getPortArmor().setHitPoints(port);
        boat.getStarboardArmor().setHitPoints(starboard);

        Hitable actual = selector.selectTarget(boat);

        assertSame(expectedTarget, actual);
    }

    private void choosePartOfFortressSpecification(Fortress fortress, int walls, int turret1, int turret2, Armor expected) {
        fortress.getWalls().setHitPoints(walls);
        fortress.getTurret1().setHitPoints(turret1);
        fortress.getTurret2().setHitPoints(turret2);

        Hitable actual = selector.selectTarget(fortress);

        assertSame(expected, actual);
    }
}

