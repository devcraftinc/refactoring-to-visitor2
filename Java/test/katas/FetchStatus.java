// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas;


import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class FetchStatus {
    private StatusEngine statusEngine;

    @Before
    public void setup() {
        statusEngine = StatusEngine.getInstance();
    }

    @Test
    public void boatStatus() {
        Boat boat = Boat.getInstance();
        boat.setLat(Any.doubleValue());
        boat.setLon(Any.doubleValue());
        boat.setHeading(Any.doubleValue());
        boat.setSailsUp(Any.yesNoValue());
        boat.setCannonBalls(Any.integerValue());
        boat.getBowArmor().setHitPoints(Any.integerValue());
        boat.getSternArmor().setHitPoints(Any.integerValue());
        boat.getPortArmor().setHitPoints(Any.integerValue());
        boat.getStarboardArmor().setHitPoints(Any.integerValue());

        String status = statusEngine.getStatus(boat);

        assertEquals(
                "Position:     " + boat.getLat() + ", " + boat.getLon() + "\r\n" +
                        "Heading:       " + boat.getHeading() + "\r\n" +
                        "Sails Up:      " + boat.getSailsUp() + "\r\n" +
                        "Ammo:          " + boat.getCannonBalls() + "\r\n" +
                        "Armor:\r\n" +
                        "  - Bow:       " + boat.getBowArmor().getHitPoints() + "\r\n" +
                        "  - Stern:     " + boat.getSternArmor().getHitPoints() + "\r\n" +
                        "  - Port:      " + boat.getPortArmor().getHitPoints() + "\r\n" +
                        "  - Starboard: " + boat.getStarboardArmor().getHitPoints() + "",
                status);
    }

    @Test
    public void fortressStatus() {
        Fortress fortress = Fortress.GetInstance();
        fortress.setLat(Any.doubleValue());
        fortress.setLon(Any.doubleValue());
        fortress.setCannonBalls(Any.integerValue());
        fortress.getWalls().setHitPoints(Any.integerValue());
        fortress.getTurret1().setHitPoints(Any.integerValue());
        fortress.getTurret2().setHitPoints(Any.integerValue());

        String status = statusEngine.getStatus(fortress);

        assertEquals(
                "Position:     " + fortress.getLat() + ", " + fortress.getLon() + "\r\n" +
                        "Ammo:          " + fortress.getCannonBalls() + "\r\n" +
                        "Armor:         " + fortress.getWalls().getHitPoints() + "\r\n" +
                        "Turret 1:      " + fortress.getTurret1().getHitPoints() + "\r\n" +
                        "Turret 2:      " + fortress.getTurret2().getHitPoints() + "",
                status);
    }

    @Test
    public void armyStatus() {
        Army army = Army.getInstance();
        army.setLat(Any.doubleValue());
        army.setLon(Any.doubleValue());
        army.setSupplies(Any.integerValue());
        army.setSoldiers(Any.integerValue());

        String status = statusEngine.getStatus(army);

        assertEquals(
                "Position:     " + army.getLat() + ", " + army.getLon() + "\r\n" +
                        "Ammo:          " + army.getSupplies() + "\r\n" +
                        "Soldiers:      " + army.getSoldiers() + "",
                status);
    }

    @Test
    public void unknownType() {
        UnknownGameObject other = new UnknownGameObject();

        String status = statusEngine.getStatus(other);

        assertEquals("(unknown)", status);
    }
}

