// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package katas;

public class Boat extends GameObject {
    private Boat() {
        bowArmor = Armor.getInstance();
        sternArmor = Armor.getInstance();
        portArmor = Armor.getInstance();
        starboardArmor = Armor.getInstance();
    }

    private double heading;

    public double getHeading() {
        return heading;
    }

    public void setHeading(double value) {
        heading = value;
    }

    private YesNo sailsUp;

    public YesNo getSailsUp() {
        return sailsUp;
    }

    public void setSailsUp(YesNo value) {
        sailsUp = value;
    }

    private int cannonBalls;

    public int getCannonBalls() {
        return cannonBalls;
    }

    public void setCannonBalls(int value) {
        cannonBalls = value;
    }

    private Armor bowArmor;

    public Armor getBowArmor() {
        return bowArmor;
    }

    private Armor sternArmor;

    public Armor getSternArmor() {
        return sternArmor;
    }

    private Armor portArmor;

    public Armor getPortArmor() {
        return portArmor;
    }

    private Armor starboardArmor;

    public Armor getStarboardArmor() {
        return starboardArmor;
    }

    public static Boat getInstance() {
        return new Boat();
    }
}

