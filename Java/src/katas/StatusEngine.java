// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package katas;

public class StatusEngine {
    private StatusEngine() {
    }

    public static StatusEngine getInstance() {
        return new StatusEngine();
    }

    public String getStatus(GameObject gameObject) {
        if (gameObject instanceof Boat) {
            Boat b = (Boat) gameObject;
            return
                    "Position:     " + b.getLat() + ", " + b.getLon() + "\r\n" +
                            "Heading:       " + b.getHeading() + "\r\n" +
                            "Sails Up:      " + b.getSailsUp() + "\r\n" +
                            "Ammo:          " + b.getCannonBalls() + "\r\n" +
                            "Armor:\r\n" +
                            "  - Bow:       " + b.getBowArmor().getHitPoints() + "\r\n" +
                            "  - Stern:     " + b.getSternArmor().getHitPoints() + "\r\n" +
                            "  - Port:      " + b.getPortArmor().getHitPoints() + "\r\n" +
                            "  - Starboard: " + b.getStarboardArmor().getHitPoints();
        }

        if (gameObject instanceof Fortress) {
            Fortress f = (Fortress) gameObject;
            return
                    "Position:     " + f.getLat() + ", " + f.getLon() + "\r\n" +
                            "Ammo:          " + f.getCannonBalls() + "\r\n" +
                            "Armor:         " + f.getWalls().getHitPoints() + "\r\n" +
                            "Turret 1:      " + f.getTurret1().getHitPoints() + "\r\n" +
                            "Turret 2:      " + f.getTurret2().getHitPoints() + "";
        }

        if (gameObject instanceof Army) {
            Army a = (Army) gameObject;
            return
                    "Position:     " + a.getLat() + ", " + a.getLon() + "\r\n" +
                            "Ammo:          " + a.getSupplies() + "\r\n" +
                            "Soldiers:      " + a.getSoldiers() + "";
        }

        return "(unknown)";
    }
}

