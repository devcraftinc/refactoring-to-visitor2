// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package katas;

public class MostImportantTargetSelector {
    private MostImportantTargetSelector() {
    }

    public static MostImportantTargetSelector getInstance() {
        return new MostImportantTargetSelector();
    }

    public Hitable selectTarget(GameObject gameObject) {
        if (gameObject instanceof Boat) {
            Boat b = (Boat) gameObject;
            Hitable weakest = b.getBowArmor();
            weakest = chooseWeaker(weakest, b.getSternArmor());
            weakest = chooseWeaker(weakest, b.getPortArmor());
            weakest = chooseWeaker(weakest, b.getStarboardArmor());

            return weakest;
        }

        if (gameObject instanceof Fortress) {
            Fortress f = (Fortress) gameObject;
            Hitable weakest = null;
            weakest = chooseWeakerButStillActive(weakest, f.getTurret1());
            weakest = chooseWeakerButStillActive(weakest, f.getTurret2());

            if (weakest == null)
                weakest = f.getWalls();

            return weakest;
        }

        if (gameObject instanceof Army)
            return (Army) gameObject;

        return null;
    }

    private Hitable chooseWeakerButStillActive(Hitable left, Hitable right) {
        if (right.getHitPoints() > 0)
            return chooseWeaker(left, right);

        return left;
    }

    private Hitable chooseWeaker(Hitable left, Hitable right) {
        if (left == null)
            return right;

        if (right.getHitPoints() < left.getHitPoints())
            return right;

        return left;
    }
}

